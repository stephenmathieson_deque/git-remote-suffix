
/**
 * Module dependencies.
 */

var path = require('path');
var fs = require('fs');
var url = require('url');
var ini = require('ini');

/**
 * Expose `setup`.
 */

module.exports = setup;

/**
 * Setup git config in `dir`.
 */

function setup(dir, suffix, fn) {
  var file = path.join(dir, '.git', 'config');
  read(file, function (err, config) {
    if (err) return fn(err);

    var remotes = Object.keys(config).filter(function (key) {
      return 'remote "' == key.substr(0, 8);
    });

    for (var i = 0, remote; remote = remotes[i]; i++) {
      var obj = url.parse(config[remote].url);
      if (!~obj.host.indexOf(suffix)) {
        obj.host += suffix;
        config[remote].url = url.format(obj);
      } else {
        remotes.splice(i, 1);
      }
    }

    var str = ini.stringify(config);
    fs.writeFile(file, str, function (err) {
      if (err) return fn(err);
      return fn(null, remotes);
    });
  });
}

/**
 * Read and parse the given ini `file`.
 *
 * @api private
 * @param {String} file
 * @param {Function} fn
 */

function read(file, fn) {
  fs.readFile(file, 'utf-8', function (err, data) {
    if (err) return fn(err);
    fn(null, ini.parse(data));
  });
}
