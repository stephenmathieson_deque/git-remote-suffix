install:
	@npm link .

uninstall:
	@npm r -g git-remote-suffix

.PHONY: install uninstall
