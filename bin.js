#!/usr/bin/env node

var setup = require('./');

var dir = process.cwd();

var suffix = process.argv[2] || '-h';

if ('--help' == suffix || '-h' == suffix) {
  console.log();
  console.log('git remote-suffix <suffix>');
  console.log();
  console.log('  Setup git config settings to point to ');
  console.log('  a custom origin path.');
  console.log();
  console.log('  Useful when stuck using multiple SSH keys');
  console.log('  because your company uses BitBucket.');
  console.log();
  process.exit();
}

setup(dir, suffix, function (err, remotes) {
  if (err) throw err;
  if (!remotes.length) {
    console.warn('No remotes to suffixed.');
  } else {
    console.log('Added suffix "%s" to remotes:', suffix);
    console.log();
    console.log('  - ' + remotes.join('\n  - '));
    console.log();
  }
});
